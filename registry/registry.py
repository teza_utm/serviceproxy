import json
import time
from model.service import Service


class Registry:
    records = []

    def add_service(self, service):
        service_address = service["address"]
        if not self.is_service_registered(service_address):
            self.records.append(Service(address=service_address,
                                        endpoints=service["endpoints"],
                                        timestamp=time.time(),
                                        service_type=service["type"]))

            print("--------------SERVICE---------------", flush=True)
            for record in self.records:
                print(record.__dict__, flush=True)
        else:
            service = self.get_record(service_address)
            service.timestamp = time.time()
        return json.dumps(service, default=lambda o: o.__dict__)

    def delete_service(self, service):
        self.records.remove(service)
        print("--------------SERVICE---------------", flush=True)
        for record in self.records:
            print(record.__dict__, flush=True)

    def is_service_registered(self, service_address):
        if any(record.address == service_address for record in self.records):
            return True
        else:
            return False

    def get_record(self, address):
        for record in self.records:
            if record.address == address:
                return record

