import time
import threading


def check_service_availability(registry):
    processing_thread = threading.Thread(target=delete_unavailable_services, args=(registry,))
    processing_thread.start()


def delete_unavailable_services(registry):
    service_cleanup_interval = 15
    while True:
        time.sleep(1)
        current_time = time.time()
        for record in registry.records:
            diff_time = current_time - record.timestamp
            if diff_time > service_cleanup_interval:
                registry.delete_service(record)
