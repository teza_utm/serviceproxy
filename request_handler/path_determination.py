def get_service_path(path, service):
    service_address = service.address
    return "http://" + service_address + path
