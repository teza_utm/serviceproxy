import requests
from request_handler.path_determination import get_service_path
from flask import abort, make_response

round_robin_registry = {}


def handle_request(request, registry):
    service = get_service(registry.records, request.path)
    full_path = get_service_path(request.full_path, service)
    print("Service request to " + full_path, flush=True)
    try:
        req = requests.request(method=request.method,
                               url=full_path,
                               data=request.data,
                               headers=request.headers,
                               allow_redirects=False)
        return req
    except Exception:
        print("-------------EXCEPTION----------------", flush=True)
        if registry.is_service_registered(service):
            registry.delete_service(service)
        return handle_request(request, registry)


def round_robin(registry):
    if len(registry) == 0:
        abort(make_response('{"message": "There are no services registered with such endpoint"}', 404))

    service_type = registry[0].service_type
    position = round_robin_registry.get(service_type)
    if not position:
        position = 0

    if position > len(registry) - 1:
        position = 0

    service = registry[position]

    position += 1
    round_robin_registry[service_type] = position

    return service


def get_service(registry, path):
    services = []
    for service in registry:
        endpoints = [endpoint["path"] for endpoint in service.endpoints if endpoint["path"] in path]
        if endpoints:
            services.append(service)
    return round_robin(services)
