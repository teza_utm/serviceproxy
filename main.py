import json
from flask import Flask, Response, request
from registry.registry import Registry
from request_handler.load_balancer import handle_request
from registry.discovery_job import check_service_availability


app = Flask(__name__)
registry = Registry()


@app.route("/register", methods=["POST"])
def register_service():
    request_payload = json.loads(request.data)
    return Response(registry.add_service(request_payload), mimetype="application/json")


@app.route('/<path:path>', methods=['GET', 'POST', 'DELETE', 'PUT', 'PATCH'])
def request_service(path):
    req = handle_request(request, registry)
    req.headers.pop('Transfer-Encoding', None)
    return Response(response=req.content, status=req.status_code, headers=req.headers.items())


if __name__ == '__main__':
    check_service_availability(registry)
    app.run(host="0.0.0.0", port=4000, debug=True, threaded=True, use_reloader=False)


